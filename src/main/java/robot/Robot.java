package robot;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Robot {
    private int clientCount  = 0;
    private boolean listen = true;

    public static void main(String[] args) {
        try {
            new Robot(args.length > 0 ? Integer.parseInt(args[0]) : 3500);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Robot(int port) throws IOException {
        ServerSocket server = new ServerSocket(port);
        while (listen) {
            RobotDispatcher handler = new RobotDispatcher(server.accept(), clientCount++);
            Thread clientThread = new Thread(handler);
            clientThread.start();
        }
    }


    /**
     * @author Jan Vesely
     * {@link RobotDispatcher} contais client connection thread.
     * Routers each service in its definition.
     */
    public static class RobotDispatcher implements  Runnable {
        private static final String LOGIN_TAG = "200 LOGIN";
        private static final String PASSWORD_TAG = "201 PASSWORD";
        private static final String OK_TAG = "202 OK";
        private static final String LOGIN_FAILED = "500 LOGIN FAILED";
        private static final String SYNTAX_ERROR_TAG = "501 SYNTAX ERROR";
        private static final String BAD_CHECKSUM = "300 BAD CHECKSUM";
        private static final String TIMEOUT = "502 TIMEOUT";

        private String robotName = null;
        private int receivedFotos = 1;
        private int robotId;


        /* Connection */
        private Socket socket;
        private InputStream in;
        private OutputStream out;


        /**
         * Initialize Runnable fot new client connection
         * @param socket the connection
         * @param robotId client connection id
         */
        public RobotDispatcher(Socket socket, final int robotId) throws IOException {
            this.robotId = robotId;
            this.socket = socket;
            this.in = socket.getInputStream();
            this.out = socket.getOutputStream();

            //Start timeout service
            new ScheduledThreadPoolExecutor(1).schedule(new Runnable() {
                @Override
                public void run() {
                    RobotDispatcher.this.die();
                }
            }, 45, TimeUnit.SECONDS);

        }

        /**
         * terminates the socket
         */
        private void die() {
            try {
                if (!socket.isClosed()){
                    writeLine(TIMEOUT);
                    socket.close();
                }
            }catch (Exception e){
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }

        @Override
        public void run() {
            try {
                communicate();
            } catch (Exception x) {
                //log("CLOSE EXP: " + x.getMessage());
            }finally {
                try {

                    //wait for socket to send all messages
                    Thread.sleep(1000);
                    if (!socket.isClosed())
                        socket.close();
                }catch (Exception ignored){
                    ignored.printStackTrace();
                }
            }
        }

        /**
         * Communication with client
         * @throws IOException
         */
        private void communicate() throws IOException {
            try {
                writeLine(LOGIN_TAG);
                byte[] login = readByteLine();
                int loginHash = 0;
                for (byte b : login) loginHash += b & 0xff;


                writeLine(PASSWORD_TAG);
                String password = readLine();

                if (loginHash == 0 || Integer.parseInt(password) != loginHash)
                    throw new NumberFormatException("Incorrect credentials.");

                robotName = new String(login);
                writeLine(OK_TAG);

            }catch (NumberFormatException e){
                writeLine(LOGIN_FAILED);
                throw new EOFException("Incorrect login");
            }

            while (true) acceptMessage();
        }

        /**
         * Read from the input stream until \r\n is reached
         * @return single line of text without \r\n at the end
         * @throws IOException if any IO occurs
         */
        private byte[] readByteLine() throws IOException{
            ArrayList<Byte> bytes = new ArrayList<>();

            byte current = 0, last = 0;
            while (last != 13 || current != 10){
                last = current;
                current = read();
                bytes.add(current);
            }

            byte[] result = new byte[bytes.size()-2];
            for (int i = 0; i < bytes.size()-2; i++)
                result[i] = bytes.get(i);

            return  result;
        }

        public String readLine() throws IOException {
            return new String(readByteLine());
        }

        /**
         * Switch for processing incoming message
         * @throws IOException
         */
        private void acceptMessage() throws IOException {
            char[] action = new char[5];
            char[] info = new char[]{'I', 'N', 'F', 'O', ' '};
            char[] foto = new char[]{'F', 'O', 'T', 'O', ' '};

            for (int i = 0; i < action.length; i++) {
                action[i] = (char) read();

                if (action[i] != info[i] &&  action[i] != foto[i]){
                    writeLine(SYNTAX_ERROR_TAG);
                    throw new EOFException("Unknown message");
                }
            }

            if (action[0] == 'I')
                processInfo();
            else
                processFoto();

        }


        /**
         * Process FOTO message
         * @throws IOException
         */
        private void processFoto() throws IOException{
            byte input;

            //Read data length
            StringBuilder builder = new StringBuilder();
            while(true) {
                input = read();
                if ((char) input == 32) break;

                builder.append((char) input);

                if (input < 48 || input > 57) {
                    writeLine(SYNTAX_ERROR_TAG);
                    throw new EOFException("foto size must be a number");
                }
            }

            //Read data, length is always parsable number
            byte[] raw = new byte[Integer.parseInt(builder.toString())];
            int rawChecksum = 0;
            for (int i = 0; i < raw.length; i++){
                raw[i] = read();
                rawChecksum +=  raw[i] & 0xff;
            }

            // get check sum
            byte[] checkBytes = new byte[4];
            for (int i = 0; i < checkBytes.length; i++) {
                checkBytes[i] = read();
            }

            int expected = ByteBuffer.wrap(checkBytes).getInt();


            if (rawChecksum != expected){
                writeLine(BAD_CHECKSUM);
                return;
            }

            writeLine(OK_TAG);

            NumberFormat formatter = new DecimalFormat("000");
            File foto = new File("/home/honza/dev/baryk/"+ robotName + "/" + formatter.format(receivedFotos) + ".jpg");
            receivedFotos++;

            foto.getParentFile().mkdirs();

            FileOutputStream stream = new FileOutputStream(foto.getPath());
            stream.write(raw);
            stream.close();
        }

        /**
         * process INFO message
         * @throws IOException
         */
        private void processInfo() throws IOException{
            String message = readLine();
            System.out.println(message);
            writeLine(OK_TAG);
        }


        /**
         * write text to the {@link OutputStream} ending with \r\n
         * @param message
         * @throws IOException
         */
        private void writeLine(String message) throws IOException {
            message += "\r\n";
            out.write(message.getBytes());
            out.flush();
        }


        /**
         * Read nex byte of data or throw {@link EOFException}
         * @return next byte of data
         * @throws IOException
         */
        private byte read() throws IOException{
            int b = in.read();

            if (b == -1){
                throw new EOFException("End of stream, close socket");
            }

            return (byte) b;
        }

        private void log(String message){
            System.out.println("Robot: " + robotId + ": " + message);
        }

    }

}
